<?php

    $connection = new mysqli("localhost", "root", "", "learnwords_db");
    if($connection->connect_error)
    {
        die("Connection failed!".$connection->connect_error);
    }
    
    $result = array('error'=>false);
    $action = '';
    
    if(isset($_GET['action']))
    {
        $action = $_GET['action'];
    }
    
    if($action == 'read')
    {
        $sql = $connection->query("SELECT * FROM users");
        $users = array();
        while($row = $sql->fetch_assoc())
        {
            array_push($users, $row);
        }
        $result['users'] = $users;
    }
    
    if($action == 'create')
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $sql = $connection->query("INSERT INTO users (name,email) VALUES('$name', '$email')");
        
        if($sql)
        {
            $result['message'] = "User added successfully";
        }
        else
        {
            $result['error'] = true;
            $result['message'] = "Failed to add user!";
        }
    }
    
    if($action == 'update')
    {
        $id = $_POST['id'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        
        $sql = $conn->query("UPDATE users SET name = '$name', email = '$email' WHERE id='$id'");
        if($sql)
        {
            $result['message'] = "User updated successfully";
        }
        else{
            $result['error'] = true;
            $result['message'] = "Failed to update user!";
        }
    }
    
    if($action == 'delete')
    {
        $id = $_POST['id'];
        
        $sql = $connection->query("DELETE FROM users WHERE id='$id'");
        
        if($sql)
        {
            $result['message'] = "User deleted successfully";
        }
        else{
            $result['error'] = true;
            $result['message'] = "Failed to delete user!";
        }
    }
    
    $connection->close();
    echo json_encode($result);
?>